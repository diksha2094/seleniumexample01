import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class InputBoxTest {
WebDriver driver;
final String URL="https://www.seleniumeasy.com/test/basic-first-form-demo.html";
final String DRIVER_PATH="/Users/macstudent/Desktop/chromedriver";
	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver",DRIVER_PATH);
 		WebDriver driver = new ChromeDriver();
 		driver.get(URL);
	}

	@After
	public void tearDown() throws Exception {
		Thread.sleep(1000);
 		TimeUnit.SECONDS.sleep(1);
 				
				driver.close();
	}

	@Test
	public void testSingleInput() throws InterruptedException{
		
 		
 		//1. Find the text box
 		WebElement textBox=driver.findElement(By.id("user-message"));
 		
 		//2. Type your msg into text box(id=user message) (send keys("..."))
 		textBox.sendKeys("Hello World !");
 		
 		

 		//3. Find the button  	form#get-input button
 		WebElement button=driver.findElement(By.cssSelector("form#get-input button"));
 		
 		//4. click on the button
 		button.click();
 		//5. find the output msg thing(id=msg)
 		WebElement outputSpan=driver.findElement(By.id("display"));
 		String outputMsg = outputSpan.getText();
 		assertEquals("Hello World !",outputMsg);
 		
	}
	
	@Test
	public void testDoubleInput()
	{
		
		//1. Find text box 1(id=sum1)
 		WebElement textBox=driver.findElement(By.id("sum1"));
 		
		//2. type 50 into textbox (.sendkeys(""))
 		textBox.sendKeys("50");
 		
        //3. Find text box 2		
 		WebElement textBox1=driver.findElement(By.id("sum2"));
 		
		//4. type 70 into textbox (.sendkeys(""))
 		textBox1.sendKeys("20");
		//5. find the button (cssselector= form#gettotal button)
 		WebElement button=driver.findElement(By.cssSelector("form#gettotal button"));
 		button.click();
 		
 		//6.click the button (.click())
 		WebElement outputSpan=driver.findElement(By.id("displayvalue"));
 		
 		//7.check the output
 		String output= outputSpan.getText();
 		assertEquals("70",output);
		
	}

}
