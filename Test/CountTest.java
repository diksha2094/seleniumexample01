import static org.junit.Assert.*;

import java.awt.Dimension;
import java.awt.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
public class CountTest {

	
		WebDriver driver;
		final String URL="https://www.webdirectory.com/Animals/";
		final String DRIVER_PATH="/Users/macstudent/Desktop/chromedriver";
			@Before
			public void setUp() throws Exception {
				System.setProperty("webdriver.chrome.driver",DRIVER_PATH);
		 		WebDriver driver = new ChromeDriver();
		 		driver.get(URL);
	}

	@After
	public void tearDown() throws Exception {
		Thread.sleep(1000);
 		TimeUnit.SECONDS.sleep(1);
 				
				driver.close();
		
	}

	@Test
	public void test() {
		List<WebElement> listOflinks = driver.findElements(By.cssSelector("table+ul li a"));
		int noOfLinks=listOflinks.size();
		assertEquals(10,noOfLinks);
		
	}

}
